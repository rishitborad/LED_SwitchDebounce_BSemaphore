

/*******Source for switch******/

#include "tasks.hpp"
#include "examples/examples.hpp"
#include "eint.h"
#include "stdio.h"
#include "gpio.hpp"
#include "core_cm3.h"

SemaphoreHandle_t Start_led,debounce;
uint8_t rising_flag = false;
void Debounce_ISR(){
   //xSemaphoreGiveFromISR(debounce,NULL);
    rising_flag = true;
}
void LED_ISR(){
   xSemaphoreGiveFromISR(Start_led,NULL);
}

class LED_task :public scheduler_task
{
    private:
        GPIO p2_wire0;
    public:
        LED_task(uint8_t priority):scheduler_task("LED_Task",1024,priority),p2_wire0(LPC1758_GPIO_Type::P2_0){

        }
                bool init(void){
                    const uint8_t Switch2_1 = 1;
                    eint3_enable_port2(Switch2_1,eint_rising_edge,LED_ISR);
                    eint3_enable_port2(Switch2_1,eint_falling_edge,Debounce_ISR);
                    p2_wire0.setAsOutput();
                    return true;
                }

                bool run(void *p)
                {
                    if(xSemaphoreTake(Start_led,999))
                    {
                       //printf("falling->\n");
                       p2_wire0.setHigh();
                    }
                    if(rising_flag == true)
                    {
                       //printf("rising\n");
                       for(int i=0;i<20;i++){};
                       NVIC_ClearPendingIRQ(EINT3_IRQn);
                       rising_flag = false;
                    }
                    else
                    {
                        p2_wire0.setLow();
                    }
                    return true;
                }
        };
int main(void)
{
    scheduler_add_task(new LED_task(PRIORITY_MEDIUM));
    vSemaphoreCreateBinary(Start_led);
    vSemaphoreCreateBinary(debounce);








/*********Source for LED*******/

SemaphoreHandle_t binsem;
void SW_LED()
{
    xSemaphoreGiveFromISR(binsem,NULL);

}


class LED_task : public scheduler_task
{

    private:
    GPIO p2_wire0;


    public:
     LED_task(uint8_t priority) :
    scheduler_task("LED_task", 2000, priority),
    p2_wire0(LPC1758_GPIO_Type::P2_0){ }

     bool init(void)
     {
     const uint8_t port2_1 = 1;
     eint3_enable_port2(port2_1, eint_rising_edge, SW_LED);
     p2_wire0.setAsOutput();
     return true;
     }

     bool run(void *p)
     {

        if(xSemaphoreTake(binsem,portMAX_DELAY))
        {
            p2_wire0.setLow();
            vTaskDelay(500);
            printf("swtch\n");
        }
        p2_wire0.setHigh();
     return true;
     }
};

int main(void)
{
    vSemaphoreCreateBinary(binsem);
    scheduler_add_task(new LED_task(PRIORITY_CRITICAL));